#include <iostream>  //Entrdas y Salidas
#include <stdlib.h> //Standard windows
#include <cstdlib> //Standard linux
#include <string> //cadenas de texto
#include <unistd.h> //Unix
#include <ctime> //Tiempo
#include <cstring> //Manipular cadenas
#include <cctype> //Manipular tipos de datos
#include <algorithm> //Algoritmos
#include <iomanip> //Manipular Entrdas y Salidas
#include <fstream> //Manipular Archivos
#ifdef defined(_WIN32) || defined(_WIN64)
    #include <window.h> //Manipular consola de windows
#else
    #include <sstream>
    #include <termios.h> // Manipular terminal linux
#endif

using namespace std;

//Prototipado de funciones
void admDatosRest();
void admCategorias();
void admProductos();
void admUsuarios();
void genPedidos();
void verUsuarios();
void verCategorias();
void verProductos();
void estadoPedidos();
void hacerPedidos();
void histoPedidos();
void addItem(string prod, int cant, int numPed);
void savePedido(time_t t, int numPed, int estado, float tot);
void genTicket(int pedido);
void cambiarEstado();
void imprimir(int pedido, float dolar, float vuelto);

// Variables Globales
const int Tam = 50, TamUsr = 4, TamCat = 10, TamProd = 50, two = 2;
int error = 0, salir = 0, contadorCat = 6, contadorProd = 5, contadorPed = 0, contadorItem = 0, contadorTicket = 0;

//Estructuras
struct restaurante {
        string nombre;
        string nit;
        string nrc;
        string dir;
        string giro;
} restaurantes[1];
struct usuario {
        string nombre;
        string password;
} usuarios[TamUsr];
struct categoria {
        int idCat;//AutoIncrementable
        string nombre;
} categorias[TamCat];
struct estadoPedido { //En proceso, cancelado, terminado
        int idEstado;//AutoIncrementable
        string nombre;
} estadoPed[two];
struct producto {
        int idProd;//AutoIncrementable
        string nombre;
        float precio;
        int cat; //idCat
} productos[TamProd];
struct pedido {
        int idPed;//AutoIncrementable
        time_t hora;
        int estado; //idEstado
        float total;
} pedidos[Tam];
struct itemPedido {
        int idItem;//AutoIncrementable
        string producto;
        int estado; //idEstado
        int cantidad;
        int numPedido; //idPed
} items[Tam];
struct ticket {
        int idTicket; //Num de Orden - AutoIncrementable
        time_t fecha;
        int pedido; //idPed
} tickets[Tam];

//Inicialiar variables globales
void iniciaVars(){
        //Info Restaurante
        restaurantes[0].nombre = "Pizza El Patrón";
        restaurantes[0].nit = "0614-270892-111-5";
        restaurantes[0].nrc = "75306-8";
        restaurantes[0].dir = "7a Av Sur Entre 1a Y 3a Calle Oriente #40 Boulevard Schafick Handal, Chalchuapa";
        restaurantes[0].giro = "Restaurantes";
        //Usuarios
        usuarios[0].nombre = "-1";

        usuarios[1].nombre = "admin";
        usuarios[1].password = "54321";
        usuarios[2].nombre = "cajero";
        usuarios[2].password = "12345";

        usuarios[3].nombre = "cocina";
        usuarios[3].password = "12345";

        //Categorias
        categorias[0].idCat = 0;
        categorias[0].nombre = "Pizzas Personales";

        categorias[1].idCat = 1;
        categorias[1].nombre = "Pizzas Medianas";

        categorias[2].idCat = 2;
        categorias[2].nombre = "Pizzas Gigantes";

        categorias[3].idCat = 3;
        categorias[3].nombre = "Postres";

        categorias[4].idCat = 4;
        categorias[4].nombre = "Entradas";

        categorias[5].idCat = 5;
        categorias[5].nombre = "Bebidas";

        //Estado de Pedido
        estadoPed[0].idEstado = 0;
        estadoPed[0].nombre = "En proceso";

        estadoPed[1].idEstado = 1;
        estadoPed[1].nombre = "Completado";

        //Productos
        productos[0].idProd = 0;
        productos[0].nombre = "Pepperoni";
        productos[0].precio = 6.00;
        productos[0].cat = 0;

        productos[1].idProd = 1;
        productos[1].nombre = "Hawaiiana";
        productos[1].precio = 6.00;
        productos[1].cat = 0;

        productos[2].idProd = 2;
        productos[2].nombre = "Pan con Ajo";
        productos[2].precio = 1.50;
        productos[2].cat = 4;

        productos[3].idProd = 3;
        productos[3].nombre = "7up";
        productos[3].precio = 0.75;
        productos[3].cat = 5;

        productos[4].idProd = 4;
        productos[4].nombre = "Tres Leches";
        productos[4].precio = 1.75;
        productos[4].cat = 3;

        productos[5].idProd = 5;
        productos[5].nombre = "Salchichas";
        productos[5].precio = 4.50;
        productos[5].cat = 0;

        productos[6].idProd = 6;
        productos[6].nombre = "Suprema";
        productos[6].precio = 4.99;
        productos[6].cat = 0;

        productos[7].idProd = 7;
        productos[7].nombre = "Vegetariana Natural";
        productos[7].precio = 4.99;
        productos[7].cat = 0;

        productos[8].idProd = 8;
        productos[8].nombre = "Jamon Extremo";
        productos[8].precio = 7.50;
        productos[8].cat = 1;

        productos[9].idProd = 9;
        productos[9].nombre = "Hawaiana";
        productos[9].precio = 7.25;
        productos[9].cat = 1;

        productos[10].idProd = 10;
        productos[10].nombre = "Jamón";
        productos[10].precio = 7.50;
        productos[10].cat = 1;

        productos[11].idProd = 11;
        productos[11].nombre = "Suprema Rica";
        productos[11].precio = 8.00;
        productos[11].cat = 1;

        productos[12].idProd = 12;
        productos[12].nombre = "vegetariana";
        productos[12].precio = 7.25;
        productos[12].cat = 1;

        productos[13].idProd = 13;
        productos[13].nombre = "Pepperoni Sabroson";
        productos[13].precio = 15.50;
        productos[13].cat = 2;

        productos[14].idProd = 14;
        productos[14].nombre = "Hawaiana";
        productos[14].precio = 15.75;
        productos[14].cat = 2;

        productos[15].idProd = 15;
        productos[15].nombre = "Jamón Picante";
        productos[15].precio = 14.50;
        productos[15].cat = 2;

        productos[16].idProd = 16;
        productos[16].nombre = "Suprema";
        productos[16].precio = 16.50;
        productos[16].cat = 2;

        productos[17].idProd = 17;
        productos[17].nombre = "Vegetariana";
        productos[17].precio = 14.00;
        productos[17].cat = 2;

        productos[18].idProd = 18;
        productos[18].nombre = "Soda";
        productos[18].precio = 1.50;
        productos[18].cat = 5;

        productos[19].idProd = 19;
        productos[19].nombre = "Te";
        productos[19].precio = 1.75;
        productos[19].cat = 5;

        productos[20].idProd = 20;
        productos[20].nombre = "Café";
        productos[20].precio = 0.75;
        productos[20].cat = 5;

        productos[21].idProd = 21;
        productos[21].nombre = "Fresco";
        productos[21].precio = 1.50;
        productos[21].cat = 5;

        productos[22].idProd = 22;
        productos[22].nombre = "Palitroques";
        productos[22].precio = 3.25;
        productos[22].cat = 4;

        productos[23].idProd = 23;
        productos[23].nombre = "Canelon";
        productos[23].precio = 2.25;
        productos[23].cat = 4;

        productos[24].idProd = 24;
        productos[24].nombre = "Cheese Pops";
        productos[24].precio = 3.50;
        productos[24].cat = 4;

        productos[25].idProd = 25;
        productos[25].nombre = "Nuditos";
        productos[25].precio = 2.00;
        productos[25].cat = 4;


        productos[26].idProd = 26;
        productos[26].nombre = "Postres";
        productos[26].precio = 2.25;
        productos[26].cat = 3;
}
//Fin Variables Globales

//Borrar pantalla en windows y linux
void clear_screen(){
#ifdef WINDOWS
        std::system("cls");
#else
        // Assume POSIX
        std::system ("clear");
#endif
} // Fin Borrar Pantalla

//Capitalizar
string capitalizar(string& str) {
        str[0] = toupper(str[0]);
        for ( string::iterator it = str.begin(); it != str.end(); it++ ) {
                if ( *it == ' ' ) {
                        *( it+1 ) = toupper( *( it+1 ) );
                } else {
                        *( it+1 ) = tolower( *( it+1 ) );
                }
        }
        return str;
}
//Fin Capitalizar

//toUpper
string toUpper(string p) {
        for (int j = 0; j < p.length(); j++) { //Convierte cadenas a mayúsculas
                p[j] = toupper(p[j]);
        }
        return p;
}
//Fin toUpper
//toLower
string toLower(string p) {
        string aux;
        for (int j = 0; j < p.length(); j++) { //Convierte cadenas a minuscula
                if (p[j] != ' ' || p[j] != 32) {
                        aux += tolower(p[j]);
                }
        }
        return aux;
}
//Fin toLower

//saveProd
void saveProd(producto prod[], string viejo, string nuevo, float prec) {
        for (int i = 0; i < TamProd; i++) {
                if (prod[i].nombre == viejo) {
                        prod[i].nombre = nuevo;
                        prod[i].precio = prec;
                }
        }
}
//fin saveProd

//isExists
bool existsUsr(string pal, usuario usr[]){
        bool exists = false;
        for (int i = 0; i < TamUsr; i++) {
                if (toUpper(pal) == toUpper(usr[i].nombre)) {
                        exists = true;
                }
        }

        return exists;
}

bool existsCat(string pal, categoria cat[]){
        bool exists = false;
        for (int i = 0; i < TamCat; i++) {
                if (toUpper(pal) == toUpper(cat[i].nombre)) {
                        exists = true;
                }
        }
        return exists;
}

bool existsProd(string pal, producto prod[]){
        bool exists = false;
        for (int i = 0; i < TamProd; i++) {
                if (toUpper(pal) == toUpper(prod[i].nombre)) {
                        exists = true;
                }
        }

        return exists;
}
//Fin isExists

// Gestionar datos de Restaurante
void admDatosRest(){
        int eleccion = 0;
        string nombre, nit, nrc, dir, giro;
        clear_screen();
        while (eleccion >= 0 && eleccion <= 6) {
                do {
                        error = 0;
                        cout << "| =============================================================================== |\n" <<
                                "| =================== ADMINISTRACION DE DATOS DEL RESTAURANTE =================== |\n" <<
                                "| =============================================================================== |\n" <<
                                "|                          01. Modificar datos                                    |\n" <<
                                "|                          02. Ver datos del restaurante                          |\n"
                                "|                          00. Regresar                                           |" << endl;
                        cout << "|                        > ";
                        cin >> eleccion;
                        if (eleccion == 0) {
                                eleccion = 7;
                        } else if (eleccion == 1) {
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                            MODIFICAR DATOS                                      |\n" <<
                                        "| =============================================================================== |\n" <<
                                        "|                             01. Nombre                                          |\n" <<
                                        "|                             02. N.I.T                                           |\n" <<
                                        "|                             03. N.R.C                                           |\n" <<
                                        "|                             04. Dirección                                       |\n" <<
                                        "|                             05. Giro                                            |\n" <<
                                        "|                             06. Todos                                           |\n" <<
                                        "|                             00. Atras                                           |" << endl;
                                cout << "|                           > ";
                                cin >> eleccion;

                                switch(eleccion) {
                                case 0:
                                        clear_screen();
                                        break;
                                case 1:
                                        cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       Nombre del Restaurante: ";
                                        getline(cin,nombre);
                                        if (nombre.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                        } else {
                                                restaurantes[0].nombre = capitalizar(nombre);
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                        break;
                                case 2:
                                        cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       N.I.T. del Restaurante: ";
                                        getline(cin,nit);
                                        if (nit.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                        } else {
                                                restaurantes[0].nit = nit;
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                        break;
                                case 3:
                                        cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       N.R.C del Restaurante: ";
                                        getline(cin,nrc);
                                        if (nrc.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                        } else {
                                                restaurantes[0].nrc = nrc;
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                        break;
                                case 4:
                                        cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       Dirección del Restaurante: ";
                                        getline(cin,dir);
                                        if (dir.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                        } else {
                                                restaurantes[0].dir = dir;
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                        break;
                                case 5:
                                        cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       Giro del Restaurante: ";
                                        getline(cin,giro);
                                        if (giro.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                        } else {
                                                restaurantes[0].giro = giro;
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                        break;
                                case 6:
NombreRest:
                                        //cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       Nombre del Restaurante: ";
                                        getline(cin, nombre);
                                        if (nombre.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                     Oops! Ha dejado algun campo en blanco!                      |"<< endl;
                                                goto NombreRest;
                                        }
NitRest:
                                        //cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       N.I.T. del Restaurante: ";
                                        getline(cin, nit);
                                        if (nit.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                     Oops! Ha dejado algun campo en blanco!                      |"<< endl;
                                                goto NitRest;
                                        }
NrcRest:
                                        //cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       N.R.C. del Restaurante: ";
                                        getline(cin, nrc);
                                        if (nrc.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                     Oops! Ha dejado algun campo en blanco!                      |"<< endl;
                                                goto NrcRest;
                                        }
DirRest:
                                        //cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       Dirección del Restaurante: ";
                                        getline(cin, dir);
                                        if (dir.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                     Oops! Ha dejado algun campo en blanco!                      |"<< endl;
                                                goto DirRest;
                                        }
GiroRest:
                                        //cin.ignore(100,'\n');
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                       Giro del Restaurante: ";
                                        getline(cin, giro);
                                        if (giro.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                     Oops! Ha dejado algun campo en blanco!                      |"<< endl;
                                                goto GiroRest;
                                        }
                                        restaurantes[0].nombre = nombre;
                                        restaurantes[0].nit = nit;
                                        restaurantes[0].nrc = nrc;
                                        restaurantes[0].dir = dir;
                                        restaurantes[0].giro = giro;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                                     Hecho!                                      |" << endl;

                                        break;
                                default:
                                        error = 1;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        break;
                                }
                        } else if (eleccion == 2) {
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                              DATOS DEL RESTAURANTE                              |\n" <<
                                        "| =============================================================================== |\n" <<
                                        "|        Nombre del Restaurante: " << restaurantes[0].nombre <<
                                        "\n|        N.I.T. del Restaurante: " << restaurantes[0].nit <<
                                        "\n|        N.R.C. del Restaurante: " << restaurantes[0].nrc <<
                                        "\n|        Dirección del Restaurante: " << restaurantes[0].dir <<
                                        "\n|        Giro del Restaurante: " << restaurantes[0].giro <<
                                        "\n| =============================================================================== |\n" <<
                                        "|                       Presione 'Enter' para continuar...                        |\n" <<
                                        "| =============================================================================== |" << endl;;
                                cin.ignore().get();
                                clear_screen();
                        } else {
                                error = 1;
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                             Oops! Opción inválida!                              |" << endl;
                        }
                } while(error == 1);
        }
} // Fin Gestionar datos

// Gestionar categorias de platos
void admCategorias(){
        int eleccion=0, i, numCat;
        string nomCat;
        clear_screen();
        while (eleccion >= 0 && eleccion <= 3) {
                do {
                        error = 0;
                        cout << "| =============================================================================== |\n" <<
                                "| =================== ADMINISTRACION DE CATEGORIAS DE PLATOS ==================== |\n" <<
                                "| =============================================================================== |\n" <<
                                "|                       01. Nueva categoria                                       |\n" <<
                                "|                       02. Modificar categoria                                   |\n" <<
                                "|                       03. Ver categorias registradas                            |\n" <<
                                "|                       00. Regresar                                              |" << endl;
                        cout << "|                     > ";
                        cin >> eleccion;
                        if (eleccion == 0) {
                                eleccion = 4;
                        } else if (eleccion == 1) {
                                clear_screen();
                                cin.ignore(100,'\n');
                                cout << "| =============================================================================== |" << endl;
                                cout << "|                    Nombre de la nueva Categoría: ";
                                getline(cin,nomCat);
                                if (nomCat.length() == 0) {
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                            Oops! No ha escrito nada!                            |"<< endl;
                                } else if (existsCat(nomCat,categorias)) {
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                  Oops! Ya existe la categoría "<< nomCat << "!" << endl;
                                } else {
                                        categorias[contadorCat].nombre = capitalizar(nomCat);
                                        contadorCat++;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                                     Hecho!                                      |" << endl;
                                }
                        } else if (eleccion == 2) {
                                clear_screen();
ModCat:
                                cout << "| =============================================================================== |\n" <<
                                        "| ===================== SELECCIONE LA CATEGORÍA A MODIFICAR ===================== |\n" <<
                                        "| =============================================================================== |" << endl;
                                for (int i = 0; i < TamCat; i++) {
                                        if (categorias[i].nombre == "") { break; }
                                        cout << "|                             " << i+1 << " - " << categorias[i].nombre << endl;

                                }
                                cout << "|                             00. Atras                                           |" << endl;
                                cout << "|                           > ";
                                cin >> numCat;
                                if (numCat == 0) {
                                        clear_screen();
                                } else if (numCat < 0 || numCat > contadorCat) {
                                        error = 1;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        goto ModCat;
                                } else {
                                        cin.ignore(100,'\n');
                                        clear_screen();
NombreCat:
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|    Nuevo nombre para la Categoría '" << categorias[numCat-1].nombre << "': ";
                                        getline(cin,nomCat);
                                        if (nomCat.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                                goto NombreCat;
                                        } else if (existsCat(nomCat,categorias)) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                  Oops! Ya existe la categoría "<< nomCat << "!" << endl;
                                                goto NombreCat;
                                        } else {
                                                categorias[numCat-1].nombre = capitalizar(nomCat);
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                }
                        } else if (eleccion == 3) {
                                verCategorias();
                        } else {
                                error = 1;
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                             Oops! Opción inválida!                              |" << endl;
                        }
                } while(error == 1);
        }
}// Fin Gestionar categorias

// Gestionar Productos
void admProductos(){
        int eleccion=0, i, numProd, numCat, resp, aux=0;
        float prec, tempPrec[50];
        string nomProd,tempProd[50];
        clear_screen();
        while (eleccion >= 0 && eleccion <= 5) {
                do {
                        error = 0;
                        cout << "| =============================================================================== |\n" <<
                                "| ======================== ADMINISTRACION DE PRODUCTOS ========================== |\n" <<
                                "| =============================================================================== |\n" <<
                                "|                         01. Nuevo producto                                      |\n" <<
                                "|                         02. Modificar producto                                  |\n" <<
                                "|                         03. Ver productos registrados                           |\n" <<
                                "|                         00. Regresar                                            |" << endl;
                        cout << "|                       > ";
                        cin >> eleccion;
                        if (eleccion == 0) {
                                eleccion = 6;
                        } else if (eleccion == 1) {
                                clear_screen();
CatProd:
                                cout << "| =============================================================================== |\n" <<
                                        "| ========================== SELECCIONE UNA CATEGORÍA =========================== |\n" <<
                                        "| =============================================================================== |" << endl;
                                for (int i = 0; i < TamCat; i++) {
                                        if (categorias[i].nombre == "") { break; }
                                        cout << "|                             " << i+1 << " - " << categorias[i].nombre << endl;

                                }
                                cout << "|                             00. Atras                                           |" << endl;
                                cout << "|                           > ";
                                cin >> numCat;
                                if (numCat == 0) {
                                        clear_screen();
                                } else if (numCat < 0 || numCat > contadorCat) {
                                        error = 1;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        goto CatProd;
                                } else {
                                        cin.ignore(100,'\n');
                                        clear_screen();
NombreProd:
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|                    Nombre del nuevo Producto: ";
                                        getline(cin,nomProd);
                                        if (nomProd.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                                nomProd = productos[contadorProd].nombre;
                                                goto NombreProd;
                                        } else if (existsProd(nomProd,productos)) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                  Oops! Ya existe el producto '"<< nomProd << "'!" << endl;
                                                nomProd = productos[contadorProd].nombre;
                                                goto NombreProd;
                                        } else {
PrecioProd:
                                                cout << "| =============================================================================== |" << endl;
                                                cout << "|                    Precio de '"<< nomProd << "': ";
                                                cin >> prec;
                                                if (prec <= 0) {
                                                        clear_screen();
                                                        cout << "| =============================================================================== |\n" <<
                                                                "|                            Oops! Error en el precio!                            |"<< endl;
                                                        goto PrecioProd;
                                                } else {
                                                        productos[contadorProd].nombre = capitalizar(nomProd);
                                                        productos[contadorProd].precio = prec;
                                                        productos[contadorProd].cat = numCat-1;
                                                        contadorProd++;
                                                        clear_screen();
                                                        cout << "| =============================================================================== |\n" <<
                                                                "|                                     Hecho!                                      |" << endl;
                                                }
                                        }
                                }
                        } else if (eleccion == 2) {
                                clear_screen();
CatModProd:
                                cout << "| =============================================================================== |\n" <<
                                        "| ========================== SELECCIONE UNA CATEGORÍA =========================== |\n" <<
                                        "| =============================================================================== |" << endl;
                                for (int i = 0; i < TamCat; i++) {
                                        if (categorias[i].nombre == "") { break; }
                                        cout << "|                             " << i+1 << " - " << categorias[i].nombre << endl;

                                }
                                cout << "|                             00. Atras                                           |" << endl;
                                cout << "|                           > ";
                                cin >> numCat;
                                if (numCat == 0) {
                                        clear_screen();
                                } else if (numCat < 0 || numCat > contadorCat) {
                                        error = 1;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        goto CatModProd;
                                } else {
                                        cin.ignore(100,'\n');
                                        clear_screen();
NombreModProd:
                                        cout << "| =============================================================================== |\n" <<
                                                "| ====================== SELECCIONE EL PRODUCTO A MODIFICAR ===================== |\n" <<
                                                "| =============================================================================== |" << endl;
                                        aux=0;
                                        for (int i = 0; i < TamProd; i++) {
                                                if (productos[i].nombre == "") { break; }
                                                if (productos[i].cat == (numCat-1)) {
                                                        cout << "|                             " << aux+1 << " - " << productos[i].nombre << endl;
                                                        tempProd[aux] = productos[i].nombre;
                                                        tempPrec[aux] = productos[i].precio;
                                                        aux++;
                                                }
                                        }
                                        cout << "|                             00. Atras                                           |" << endl;
                                        cout << "|                           > ";
                                        cin >> numProd;

                                        if (numProd == 0) {
                                                clear_screen();
                                        } else if (numProd < 0 || numProd > (aux+1)) {
                                                error = 1;
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                             Oops! Opción inválida!                              |" << endl;
                                                goto NombreModProd;
                                        } else {
NewNombreProd:
                                                cout << "| =============================================================================== |\n" <<
                                                        "|    Modificar nombre de '" << tempProd[numProd-1] << "'?\n|    1. Si\n|    2. No" << endl;
                                                cout << "|  > ";
                                                cin >> resp;
                                                if (resp == 1) {
                                                        clear_screen();
                                                        cout << "| =============================================================================== |" << endl;
                                                        cout << "|    Nuevo nombre para el producto '" << tempProd[numProd-1] << "': ";
                                                        cin.ignore(100,'\n');
                                                        getline(cin,nomProd);
                                                        if (nomProd.length() == 0) {
                                                                clear_screen();
                                                                cout << "| =============================================================================== |\n" <<
                                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                                                goto NewNombreProd;
                                                        } else if (existsProd(nomProd,productos)) {
                                                                clear_screen();
                                                                cout << "| =============================================================================== |\n" <<
                                                                        "|                  Oops! Ya existe el producto '"<< nomProd << "'!" << endl;
                                                                goto NewNombreProd;
                                                        } else {
                                                                saveProd(productos,tempProd[numProd-1],capitalizar(nomProd),tempPrec[numProd-1]);
                                                                tempProd[numProd-1] = capitalizar(nomProd);
                                                                clear_screen();
                                                                cout << "| =============================================================================== |\n" <<
                                                                        "|                                     Hecho!                                      |" << endl;
                                                        }
                                                }
                                                cout << "| =============================================================================== |\n" <<
                                                        "|    Modificar precio de '" << tempProd[numProd-1] << "'?\n|    1. Si\n|    2.No" << endl;
                                                cout << "|  > ";
                                                cin >> resp;
                                                if (resp == 1) {
NewPrecioModProd:
                                                        clear_screen();
                                                        cout << "| =============================================================================== |" << endl;
                                                        cout << "|               Nuevo precio de '"<< tempProd[numProd-1] << "': ";
                                                        cin >> prec;
                                                        if (prec <= 0) {
                                                                clear_screen();
                                                                cout << "| =============================================================================== |\n" <<
                                                                        "|                            Oops! Error en el precio!                            |"<< endl;
                                                                goto NewPrecioModProd;
                                                        } else {
                                                                tempPrec[numProd-1] = prec;
                                                                saveProd(productos,tempProd[numProd-1],capitalizar(nomProd),tempPrec[numProd-1]);
                                                                clear_screen();
                                                                cout << "| =============================================================================== |\n" <<
                                                                        "|                                     Hecho!                                      |" << endl;
                                                        }
                                                } else {
                                                        clear_screen();
                                                }
                                        }
                                }
                        } else if (eleccion == 3) {
                                verProductos();
                        } else {
                                error = 1;
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                             Oops! Opción inválida!                              |" << endl;
                        }
                } while(error == 1);
        }
}// Fin Gestionar productos

// Gestionar usuarios
void admUsuarios() {
        int eleccion=0, i, numUsr;
        string nomUsr;
        clear_screen();
        while (eleccion >= 0 && eleccion <= 5) {
                do {
                        error = 0;
                        cout << "| =============================================================================== |\n" <<
                                "| ========================= ADMINISTRACION DE USUARIOS ========================== |\n" <<
                                "| =============================================================================== |\n" <<
                                "|                         01. Modificar nombre usuario                            |\n" <<
                                "|                         02. Ver usuarios registrados                            |\n" <<
                                "|                         00. Regresar                                            |" << endl;
                        cout << "|                       > ";
                        cin >> eleccion;
                        if (eleccion == 0) {
                                eleccion = 6;
                        } else if (eleccion == 1) {
                                clear_screen();
                                cin.ignore(100,'\n');
NombreUsr:
                                cout << "| =============================================================================== |\n" <<
                                        "| ======================= SELECCIONE EL NOMBRE A MODIFICAR ====================== |\n" <<
                                        "| =============================================================================== |" << endl;
                                for (int i = 0; i < TamUsr; i++) {
                                        if (usuarios[i].nombre == "") { break; }
                                        if (usuarios[i].nombre != "-1") {
                                                cout << "|                             0" << i << ". " << usuarios[i].nombre << endl;
                                        }
                                }
                                cout << "|                             00. Atras                                           |" << endl;
                                cout << "|                           > ";
                                cin >> numUsr;

                                if (numUsr == 0) {
                                        clear_screen();
                                } else if (numUsr < 0 || numUsr > TamUsr) {
                                        error = 1;
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        goto NombreUsr;
                                } else {
NewNombreUsr:
                                        cout << "| =============================================================================== |" << endl;
                                        cout << "|    Nuevo nombre de usuario para '" << usuarios[numUsr].nombre << "': ";
                                        cin.ignore(100,'\n');
                                        getline(cin,nomUsr);
                                        if (nomUsr.length() == 0) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                            Oops! No ha escrito nada!                            |"<< endl;
                                                goto NewNombreUsr;
                                        } else if (existsUsr(nomUsr,usuarios)) {
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                  Oops! Ya existe el usuario '"<< nomUsr << "'!" << endl;
                                                goto NewNombreUsr;
                                        } else {
                                                usuarios[numUsr].nombre = toLower(nomUsr);
                                                clear_screen();
                                                cout << "| =============================================================================== |\n" <<
                                                        "|                                     Hecho!                                      |" << endl;
                                        }
                                }

                        } else if (eleccion == 2) {
                                verUsuarios();
                        } else {
                                error = 1;
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                             Oops! Opción inválida!                              |" << endl;
                        }
                } while(error == 1);
        }
}// Fin Gestionar usuarios

//Pedidos
void genPedidos(){
        int numCat, aux=0, numProd, cant, item, resp, tempProdId[50];
        float subtotal=0, total=0, tempPrec[50];
        std::stringstream stream;
        string cadena, tempProd[50];
        time_t fecha;
        clear_screen();
        item=0;
CatLlevar:
        clear_screen();
        cout << "| =============================================================================== |\n" <<
                "| ========================== SELECCIONE UNA CATEGORÍA =========================== |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < TamCat; i++) {
                if (categorias[i].nombre == "") { break; }
                cout << "|       " << i+1 << " - " << categorias[i].nombre << endl;

        }
        cout << "|       00. Atras                                           |" << endl;
        cout << "|     > ";
        cin >> numCat;
        if (numCat == 0) {
                clear_screen();
        } else if (numCat < 0 || numCat > contadorCat) {
                error = 1;
                clear_screen();
                cout << "| =============================================================================== |\n" <<
                        "|                             Oops! Opción inválida!                              |" << endl;
                goto CatLlevar;
        } else {
                cin.ignore(100,'\n');
                clear_screen();
ProdLlevar:
                cout << "| =============================================================================== |\n" <<
                        "| ============================ SELECCIONE EL PRODUCTO =========================== |\n" <<
                        "| =============================================================================== |" << endl;
                aux=0;
                for (int i = 0; i < TamProd; i++) {
                        if (productos[i].nombre == "") { break; }
                        if (productos[i].cat == (numCat-1)) {
                                tempProdId[aux] = productos[i].idProd;
                                tempProd[aux] = productos[i].nombre;
                                tempPrec[aux] = productos[i].precio;
                                cout << "|       (" << aux+1 << ")\t-\t$" << fixed << setprecision(2) << productos[i].precio << "\t-\t"<< productos[i].nombre << endl;
                                aux++;
                        }
                }
                cout << "|       00. Atras                                            |\n" <<
                        "|       -1. Cobrar                                           |" << endl;
                cout << "|     > ";
                cin >> numProd;

                if (numProd == -1) {
                        clear_screen();
                        cout << "Se ha cobrado.";
                } else if (numProd == 0) {
                        clear_screen();
                        goto CatLlevar;
                } else if (numProd < 0 || numProd > (aux+1)) {
                        error = 1;
                        clear_screen();
                        cout << "| =============================================================================== |\n" <<
                                "|                             Oops! Opción inválida!                              |" << endl;
                        goto ProdLlevar;
                } else {
CantProdLlevar:
                        cout << "| =============================================================================== |" << endl;
                        cout << "|               Cantidad de '"<< tempProd[numProd-1] << "': ";
                        cin >> cant;
                        if (cant <= 0) {
                                clear_screen();
                                cout << "| =============================================================================== |\n" <<
                                        "|                            Oops! Error en el precio!                            |"<< endl;
                                goto CantProdLlevar;
                        } else {
                                clear_screen();
                                subtotal = cant * tempPrec[numProd-1];
                                total += subtotal;
                                addItem(tempProd[numProd-1],cant,contadorPed);
                                stream << "|       " << cant << "\t$" << fixed << setprecision(2) << tempPrec[numProd-1] << "\t" << tempProd[numProd-1] << "\t" << "\t$" << subtotal << endl;
                                cadena = stream.str();
                                cout << "| =============================================================================== |\n" <<
                                        "|                                     Pagar ya?                                   |\n" <<
                                        "| =============================================================================== |\n" << cadena <<
                                        "|       Total: $" << fixed << setprecision(2) << total <<
                                        "\n| =============================================================================== |\n" << endl;
                                cout << "|       1. Pagar\n|       2. Agregar otro producto\n|     > ";
                                cin >> resp;
                                if (resp == 1) {
                                        //pagarYA
                                        fecha = time(0);
                                        savePedido(fecha,contadorPed,0,total);
                                        genTicket(contadorPed);
                                        contadorPed++;

                                } else {
                                        goto CatLlevar;
                                }
                        }

                }
        }
}
// Fin Pedidos
void addItem(string prod, int cant, int numPed){
        //items
        for (int j = 0; j < TamProd; j++) {
                if (productos[j].nombre == "") { break; }
                if (productos[j].nombre == prod) {
                        items[contadorItem].producto = productos[j].nombre;
                        items[contadorItem].cantidad = cant;
                        items[contadorItem].numPedido = numPed;
                        contadorItem++;
                }

        }
}

void savePedido(time_t t, int numPed, int estado, float tot){
        pedidos[numPed].hora = t;
        pedidos[numPed].estado = 0;
        pedidos[numPed].idPed = numPed;
        pedidos[numPed].total = tot;
}
void genTicket(int pedido) { //xxx
        float dolar, vuelto;
        int aux;
        clear_screen();
Pagar:
        cout << "| =============================================================================== |\n" <<
                "| =============================== GENERAR TICKET ================================ |\n" <<
                "| =============================================================================== |" << endl;
        cout << "|\t" << "Fecha y hora: " << ctime(&pedidos[pedido].hora);
        cout << "|\t" << "Numero de Pedido: " << pedidos[pedido].idPed+1 << endl;
        for(itemPedido its : items) {
                if (its.producto == "") { break; }
                if (pedidos[pedido].idPed == its.numPedido) {
                        for(producto p : productos) {
                                if (its.producto == p.nombre ) {
                                        aux = p.idProd;
                                }
                        }
                        cout << "|\t" << its.cantidad << "\t" << its.producto << "\t$" << fixed << setprecision(2) << productos[aux].precio << "\t$" << fixed << setprecision(2) << (its.cantidad * productos[aux].precio) << endl;
                }
        }
        cout << "|\tTotal: $" << pedidos[pedido].total << endl;
        cout << "| =============================================================================== |" << endl;
        cout << "\tPaga con: $";
        cin >> dolar;
        vuelto = dolar - pedidos[pedido].total;
        if (vuelto < 0) {
                clear_screen();
                cout << "| =============================================================================== |\n" <<
                        "|                            Oops! Error en el pago!                              |"<< endl;
                goto Pagar;
        } else {
                cout << "\tCambio: $" << fixed << setprecision(2) << vuelto << endl;
                cout << "| =============================================================================== |\n" <<
                        "|                      Presione 'Enter' para continuar...                         |\n" <<
                        "| =============================================================================== |" << endl;;
                cin.ignore().get();
                imprimir(pedido,dolar,vuelto);
                clear_screen();
        }
}

//ver usuarios
void verUsuarios(){
        clear_screen();
        cout << "| =============================================================================== |\n" <<
                "| ============================ USUARIOS REGISTRADOS ============================= |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < TamUsr; i++) {
                if (usuarios[i].nombre == "") { break; }
                if (usuarios[i].nombre != "-1") {
                        cout << "|                             " << usuarios[i].nombre << endl;
                }
        }
        cout << "| =============================================================================== |\n" <<
                "|                      Presione 'Enter' para continuar...                        |\n" <<
                "| =============================================================================== |" << endl;;
        cin.ignore().get();
        clear_screen();
}
//fin ver usuarios

// Ver Categoria de Platos
void verCategorias(){
        clear_screen();
        cout << "| =============================================================================== |\n" <<
                "| =========================== CATEGORIAS REGISTRADAS ============================ |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < TamCat; i++) {
                if (categorias[i].nombre == "") { break; }
                cout << "|                             " << categorias[i].nombre << endl;
        }
        cout << "| =============================================================================== |\n" <<
                "|                      Presione 'Enter' para continuar...                         |\n" <<
                "| =============================================================================== |" << endl;;
        cin.ignore().get();
        clear_screen();
}// Fin ver categoria
// Ver Platos Disponibles
void verProductos(){
        clear_screen();
        cout << "| =============================================================================== |\n" <<
                "| ============================ PRODUCTOS REGISTRADOS ============================ |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < TamCat; i++) {
                if (categorias[i].nombre == "") { break; }
                cout << "|                             " << toUpper(categorias[i].nombre) << endl;
                for (int j = 0; j < TamProd; j++) {
                        if (categorias[i].idCat == productos[j].cat) {
                                if (productos[j].nombre != "") {
                                        cout << "|                             $" << fixed << setprecision(2) << productos[j].precio << " - " << productos[j].nombre << endl;
                                }

                        }
                }
                cout << endl;
        }

        cout << "| =============================================================================== |\n" <<
                "|                      Presione 'Enter' para continuar...                         |\n" <<
                "| =============================================================================== |" << endl;;
        cin.ignore().get();
        clear_screen();
}// Fin ver platos

// estadoPedidos
void estadoPedidos(){

        clear_screen();
        cout << "| =============================================================================== |\n" <<
                "| ============================= PEDIDOS PENDIENTES ============================== |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < contadorPed; i++) {
                if (pedidos[i].estado == 0) {
                        cout << "|\t" << "Fecha y hora: " << ctime(&pedidos[i].hora);
                        cout << "|\t" << "Numero de Pedido: " << pedidos[i].idPed+1 << endl;
                        for(itemPedido its : items) {
                                if (its.producto == "") { break; }
                                if (pedidos[i].idPed == its.numPedido && its.estado == 0) {
                                        cout << "|\t" << its.cantidad << "\t" << its.producto << endl;
                                }
                        }
                }
                cout << "| =============================================================================== |" << endl;
        }

        cout << "| =============================================================================== |\n" <<
                "|                      Presione 'Enter' para continuar...                         |\n" <<
                "| =============================================================================== |" << endl;;
        cin.ignore().get();
        clear_screen();
}
// fin estadoPedidos

//cambiar estado Pedidos
void cambiarEstado(){
        int num;
        clear_screen();
CamEstado:
        cout << "| =============================================================================== |\n" <<
                "| ======================= SELECCIONE EL PEDIDO COMPLETADO ======================= |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < contadorPed; i++) {
                if (pedidos[i].estado == 0) {
                        cout << "|\t" << "Fecha y hora: " << ctime(&pedidos[i].hora);
                        cout << "|\t" << "Numero de Pedido: " << pedidos[i].idPed+1 << endl;
                        for(itemPedido its : items) {
                                if (its.producto == "") { break; }
                                if (pedidos[i].idPed == its.numPedido && its.estado == 0) {
                                        cout << "|\t" << its.cantidad << "\t" << its.producto << endl;
                                }
                        }
                }
                cout << "| =============================================================================== |" << endl;
        }
        cout << "|\t0. Atras\n|\t>";
        cin >> num;

        if (num == 0) {
                clear_screen();
        } else if (num < 0 || num > contadorPed) {
                clear_screen();
                cout << "| =============================================================================== |\n" <<
                        "|                             Oops! Opción inválida!                              |" << endl;
                goto CamEstado;
        } else {
                pedidos[num-1].estado = 1;
                clear_screen();
                cout << "| =============================================================================== |\n" <<
                        "|                                     Hecho!                                      |" << endl;
        }
}
//fin cambiar estado pedidos

// hacerPedidos
void hacerPedidos(){

}
// fin hacerPedidos

//historial de Pedidos
void histoPedidos(){
        string auxEst;
        int aux;
        clear_screen();
        cout << "| =============================================================================== |\n" <<
                "| ============================ HISTORIAL DE PEDIDOS ============================= |\n" <<
                "| =============================================================================== |" << endl;
        for (int i = 0; i < contadorPed; i++) {
                cout << "|\t" << "Fecha y hora: " << ctime(&pedidos[i].hora);
                cout << "|\t" << "Numero de Pedido: " << pedidos[i].idPed+1 << endl;
                for(itemPedido its : items) {
                        if (its.producto == "") { break; }
                        if (pedidos[i].idPed == its.numPedido) {
                                for(producto p : productos) {
                                        if (its.producto == p.nombre ) {
                                                aux = p.idProd;
                                        }
                                }
                                cout << "|\t" << its.cantidad << "\t" << its.producto << "\t$" << fixed << setprecision(2) << productos[aux].precio << "\t$" << fixed << setprecision(2) << (its.cantidad * productos[aux].precio) << endl;
                        }
                }
                cout << "|\tTotal: $" << pedidos[i].total << endl;
                cout << "|\tEstado de Pedido: " << estadoPed[pedidos[i].estado].nombre << endl;
                cout << "| =============================================================================== |" << endl;
        }

        cout << "| =============================================================================== |\n" <<
                "|                      Presione 'Enter' para continuar...                         |\n" <<
                "| =============================================================================== |" << endl;;
        cin.ignore().get();
        clear_screen();
}
//fin historial de Pedidos

//imprimir
void imprimir(int pedido, float dolar, float vuelto){//xxx
        int aux;
        stringstream correlativo;
        correlativo << pedido;
        string nombre = "ticket" + correlativo.str() + ".txt";

        ofstream fs(nombre);

        fs << "\t\t" << restaurantes[0].nombre << endl;
        fs << "\t\t" << "NRC: " << restaurantes[0].nrc << endl;
        fs << "\t\t" << "NIT: " << restaurantes[0].nit << endl;
        fs << "\t\t" << "GIRO: " << restaurantes[0].giro << endl;
        fs << " " << "DIRECCION: " << restaurantes[0].dir << endl;
        fs << endl;
        fs << "\t\t" << "CAJA No. 001" << endl;
        fs << "   " << "FECHA: " << ctime(&pedidos[pedido].hora) << endl;
        fs << "\t\tCAJERO(A): 001" << endl;
        fs << " -----------------------------------------" << endl;
        fs << "\t*CANT*\t*PRODUCTO*\t*VALOR*" << endl;
        for(itemPedido its : items) {
                if (its.producto == "") { break; }
                if (pedidos[pedido].idPed == its.numPedido) {
                        for(producto p : productos) {
                                if (its.producto == p.nombre ) {
                                        aux = p.idProd;
                                }
                        }
                        fs <<  "\t" << its.cantidad << "\t" << its.producto << "\t$" << fixed << setprecision(2) << productos[aux].precio << endl;
                }
        }
        fs << " -----------------------------------" << endl;
        fs << "\t\tVenta Total\t$" << pedidos[pedido].total<< endl;
        fs << "\t\tEfectivo\t$" << dolar << endl;
        fs << "\t\tCambio\t$" << vuelto << endl;
        fs << endl;
        fs << "\t\tVendedor: 001" << endl;
        fs << endl;
        fs << "\t*** GRACIAS POR SU COMPRA ***" << endl;
        fs << endl;
        fs << " Fecha Autorizacion  : 23/02/2017" << endl;
        fs << " Rango Autorizado del	1 al 50000" << endl;
        fs.close();
}
// fin imprimir


//Menu Principal
void menu(string user){
        int eleccion = 0;
        clear_screen();
        cout << "Bienvenido " << user << endl;
        while (eleccion >= 0 && eleccion <= 12) {
                do {
                        error = 0;
                        clear_screen(); //AQUIMOD
MainMenu:
                        if (user == usuarios[1].nombre) {
                                cout << "| =============================================================================== |\n" <<
                                        "| ============================ SELECCIONE UNA OPCION ============================ |\n" <<
                                        "| ===========================        GESTIONAR        =========================== |\n" <<
                                        "|                            01. Datos del restaurante                            |\n" <<
                                        "|                            02. Categorias de Productos                          |\n" <<
                                        "|                            03. Productos                                        |\n" <<
                                        "|                            04. Usuarios                                         |\n" <<
                                        "|                            05. Pedidos                                          |\n" <<
                                        "|                            06. Estado de Pedidos                                |\n" <<
                                        "| ===========================        CONSULTAR        =========================== |\n" <<
                                        "|                            07. Usuarios Registrados                             |\n" <<
                                        "|                            08. Categorías de Productos                          |\n" <<
                                        "|                            09. Productos Disponibles                            |\n" <<
                                        "|                            10. Estado de Pedidos                                |\n" <<
                                        "|                            11. Historial de Pedidos                             |\n" <<
                                        "| ===========================        SALIR            =========================== |\n" <<
                                        "|                            12. Cerrar Sesión                                    |\n" <<
                                        "|                            00. Salir                                            |" << endl;
                        } else if (user == usuarios[2].nombre) {
                                cout << "| =============================================================================== |\n" <<
                                        "| ============================ SELECCIONE UNA OPCION ============================ |\n" <<
                                        "| ===========================        GESTIONAR        =========================== |\n" <<
                                        "|                            05. Pedidos                                          |\n" <<
                                        "| ===========================        CONSULTAR        =========================== |\n" <<
                                        "|                            08. Categorías de Productos                          |\n" <<
                                        "|                            09. Productos Disponibles                            |\n" <<
                                        "|                            10. Estado de Pedidos                                |\n" <<
                                        "| ===========================        SALIR            =========================== |\n" <<
                                        "|                            12. Cerrar Sesión                                    |\n" <<
                                        "|                            00. Salir                                            |" << endl;
                        } else {
                                cout << "| =============================================================================== |\n" <<
                                        "| ============================ SELECCIONE UNA OPCION ============================ |\n" <<
                                        "| ===========================        GESTIONAR        =========================== |\n" <<
                                        "|                            06. Estado de Pedidos                                |\n" <<
                                        "| ===========================        CONSULTAR        =========================== |\n" <<
                                        "|                            10. Estado de Pedidos                                |\n" <<
                                        "|                            11. Historial de Pedidos                             |\n" <<
                                        "| ===========================        SALIR            =========================== |\n" <<
                                        "|                            12. Cerrar Sesión                                    |\n" <<
                                        "|                            00. Salir                                            |" << endl;
                        }
                        cout << "|                          > ";
                        cin >> eleccion;
                        if (user == usuarios[2].nombre) {
                                if (eleccion != 0 && eleccion != 5 && eleccion != 8 && eleccion != 9 && eleccion != 10 && eleccion != 12) {
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        goto MainMenu;
                                }
                        } else if (user == usuarios[3].nombre) {
                                if (eleccion != 0 && eleccion != 6 && eleccion != 10 && eleccion != 11 && eleccion != 12) {
                                        clear_screen();
                                        cout << "| =============================================================================== |\n" <<
                                                "|                             Oops! Opción inválida!                              |" << endl;
                                        goto MainMenu;
                                }
                        }

                        switch(eleccion) {
                        case 0: // Retorna 0
                                clear_screen();
                                salir = 1;
                                eleccion = 17;
                                break;
                        case 1:
                                admDatosRest();
                                break;
                        case 2:
                                admCategorias();
                                break;
                        case 3:
                                admProductos();
                                break;
                        case 4:
                                admUsuarios();
                                break;
                        case 5:
                                genPedidos();
                                break;
                        case 6:
                                cambiarEstado();
                                break;
                        case 7:
                                verUsuarios();
                                break;
                        case 8:
                                verCategorias();
                                break;
                        case 9:
                                verProductos();
                                break;
                        case 10:
                                estadoPedidos();
                                break;
                        case 11:
                                histoPedidos();
                                break;
                        case 12:
                                eleccion = 17;
                                break;
                        default:
                                clear_screen();
                                error = 1;
                                cout << "| =============================================================================== |\n" <<
                                        "|                             Oops! Opción inválida!                              |" << endl;
                                break;
                        }
                } while(error == 1);
        }
} // Fin Menu Principal Admin

//Hidden Passwd
string hiddenPass(){
        termios oldt;
        tcgetattr(STDIN_FILENO, &oldt);
        termios newt = oldt;
        newt.c_lflag &= ~ECHO;
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);

        string p;
        getline(cin, p);

        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

        return p;
}
//Fin Hidden Passwd


// Login
void login(){
        string username, password;
        clear_screen();

        do {
                error = 0;
                cout << "| ============================================================================== |\n" <<
                        "| ============================     ¡BIENVENIDO!      =========================== |\n" <<
                        "| ============================================================================== |" << endl;
                cout << "|          Nombre de usuario: ";
                cin >> username;
                if (username == usuarios[0].nombre) { salir = 1; break; }
                cin.ignore(100, '\n'); //Evita que getline() tome como valor el salto de línea.
                cout << "| ============================================================================== |" << endl;
                cout << "|          Contraseña (no se mostrará): ";
                password = hiddenPass();

                if(username == usuarios[1].nombre) {
                        if (password == usuarios[1].password) {
                                menu(username);
                        } else {
                                clear_screen();
                                cout << "| ============================================================================== |\n" <<
                                        "|                          Oops! Contraseña incorrecta!                         |" << endl;
                                error = 1;
                        }
                } else if(username == usuarios[2].nombre) {
                        if (password == usuarios[2].password) {
                                menu(username);
                        } else {
                                clear_screen();
                                cout << "| ============================================================================== |\n" <<
                                        "|                          Oops! Contraseña incorrecta!                         |" << endl;
                                error = 1;
                        }
                } else if (username == usuarios[3].nombre) {
                        if (password == usuarios[3].password) {
                                menu(username);
                        } else {
                                clear_screen();
                                cout << "| ============================================================================== |\n" <<
                                        "|                          Oops! Contraseña incorrecta!                         |" << endl;
                                error = 1;
                        }
                } else {
                        clear_screen();
                        cout << "| ============================================================================== |\n" <<
                                "|                          Oops! Usuario no registrado!                         |" << endl;
                        error = 1;
                }
        } while(error == 1);
} // Fin Login

//Main
int main(){
        iniciaVars();
        do {
                login();
        } while (salir == 0);
        //system("pause");
        return 0;
} // Fin Main
